<!DOCTYPE html>
<html lang="en">

<head>
    <title>Judul website</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="node_modules/bootstrap/dist/umd/popper.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"> </script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Sofia">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@700&display=swap" rel="stylesheet">
</head>

<body id="beranda">
    <div class="row">
        <div class="container-fluid" style="background: url('img/bg.jpg'); padding-bottom: 30px;">
            <div class="container">
                <?php include 'nav.php' ?>
            </div>

            <!-- <div class="container">
                <div class="row justify-content-end">
                    <div class="col-lg-4">
                        <p>
                        <h1 style="font-family: 'Sofia', sans-serif; font-size: 35px;
                            margin-top: 20%">Motto</h1>
                        </p>
                        <p>
                        <h4 style="font-family: 'Sofia', sans-serif;
                            text-align: center">"Perlindungan yang Amanah dan
                            Terpercaya"</h4>
                        </p>
                    </div>
                    <div class="col-lg-8" style="margin-top: 10%">
                        <?php include 'carousel.php' ?>
                    </div>
                </div>

            </div> -->

        </div>
    </div>

    <div class="container" style="margin-top: 30px" id="produk">
        <h3 style="text-align: center;">Produk Perusahaan</h3>
        <div class="container text-center">
            <?php include 'product.php'; ?>
        </div>
    </div>

    <div class="container" style="margin-top: 30px" id="tentang_kami">
        <h3 style="text-align: center;">Tentang Kami</h3>
        <div class="container text-center">
            <?php include 'about.php'; ?>
        </div>
    </div>

    <?php include 'footer.php' ?>

</body>

</html>
<script src="node_modules/bootstrap/dist/js/jquery-1.11.0.js"></script>
<script src="node_modules/bootstrap/dist/js/bootstrap.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('dropdown-toggle').dropdown()
});
</script>