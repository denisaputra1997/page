<div class="row" style="margin-top: 30px">
    <div class="col-lg-2 mt-4">
        <div class="container" id="pop-card">
            <img class="card-img-top" src="node_modules/bootstrap-icons/icons/building.svg" alt="Card image">
            <a href="#">
                <div class="overlay">Profil</div>
            </a>
        </div>
    </div>
    <div class="col-lg-2 mt-4">
        <div class="container" id="pop-card">
            <img class="card-img-top" src="node_modules/bootstrap-icons/icons/people.svg" alt="Card image">
            <a href="#">
                <div class="overlay">Tenaga Ahli</div>
            </a>
        </div>
    </div>
    <div class="col-lg-2 mt-4">
        <div class="container" id="pop-card">
            <img class="card-img-top" src="node_modules/bootstrap-icons/icons/person-badge.svg" alt="Card image">
            <a href="#">
                <div class="overlay">Keanggotaan Asosiasi</div>
            </a>
        </div>
    </div>
    <div class="col-lg-2 mt-4">
        <div class="container" id="pop-card">
            <img class="card-img-top" src="node_modules/bootstrap-icons/icons/person-badge.svg" alt="Card image">
            <a href="#">
                <div class="overlay">Izin Usaha</div>
            </a>
        </div>
    </div>
    <div class="col-lg-2 mt-4">
        <div class="container" id="pop-card">
            <img class="card-img-top" src="node_modules/bootstrap-icons/icons/person-badge.svg" alt="Card image">
            <a href="#">
                <div class="overlay">Struktur</div>
            </a>
        </div>
    </div>
    <div class="col-lg-2 mt-4">
        <div class="container" id="pop-card">
            <img class="card-img-top" src="node_modules/bootstrap-icons/icons/person-badge.svg" alt="Card image">
            <a href="#">
                <div class="overlay">Laporan Keuangan</div>
            </a>
        </div>
    </div>
    <div class="col-lg-2 mt-4">
        <div class="container" id="pop-card">
            <img class="card-img-top" src="node_modules/bootstrap-icons/icons/person-badge.svg" alt="Card image">
            <a href="#">
                <div class="overlay">Manajemen</div>
            </a>
        </div>
    </div>
    <div class="col-lg-2 mt-4">
        <div class="container" id="pop-card">
            <img class="card-img-top" src="node_modules/bootstrap-icons/icons/person-badge.svg" alt="Card image">
            <a href="#">
                <div class="overlay">Penghargaan</div>
            </a>
        </div>
    </div>

</div>

<style>
.col-lg-4 {
    margin-top: 20px;
}

<style>* {
    box-sizing: border-box;
}

/* Container needed to position the overlay. Adjust the width as needed */
#pop-card {
    position: relative;
    width: 100%;
}

/* Make the image to responsive */
#pop-card .card-img-top {
    display: block;
    width: 123%;
    height: auto;
    border-radius: 10px;
}

/* Make the hover at box image */
#pop-card:hover .card-img-top {
    width: 142%;
    box-shadow: 3px 3px 3px 1px grey;
}

/* The overlay effect - lays on top of the container and over the image */
.overlay {
    position: absolute;
    bottom: 0;
    background: rgb(0, 0, 0);
    background: rgba(0, 0, 0, 0.5);
    /* Black see-through */
    color: #f1f1f1;
    width: 100%;
    transition: .5s ease;
    opacity: 0;
    color: white;
    font-size: 20px;
    padding: 20px;
    text-align: center;
    border-radius: 0px 0px 10px 10px;
}

/* When you mouse over the container, fade in the overlay title */
#pop-card:hover .overlay {
    opacity: 1;
}
</style>
</style>