<div class="container" style="font-size: 11px">
    <div class="row">
        <div class="col-lg-4">
            <div class="row">
                <div class="col-lg-9" id="hubungi_kami">
                    <p>
                    <h6>Hubungi Kami</h6>
                    </p>
                    <p>
                        Jl. Sultan Agung No. 12A
                        <br> Kel. Guntur, Kec. Setiabudi,
                        <br>Jakarta Selatan 12980
                    </p>
                    <div class="row">
                        <div class="col-lg-6 col-4">
                            <ul>
                                <li>Senin - Jum'at</li>
                                <li>Sabtu</li>
                                <li>Minggu</li>
                            </ul>
                        </div>
                        <div class="col-lg-6 col-4">
                            <p>
                                : 08.00 - 17.00 WIB
                                <br>: Tutup
                                <br>: Tutup
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-9">
                        <hr width="50%">
                        <p>
                        <h6>Layanan Peserta</h6>
                        </p>
                        <div class="row">
                            <div class="col-lg-2 col-3">

                                <p> Telp
                                    <br> Fax
                                    <br>SMS
                                    <br>Email
                                </p>
                            </div>
                            <div class="col-lg-9 col-6">
                                <p>
                                    : (021) 83790999
                                    <br>: (021) 83705234
                                    <br>: (081) 222 999 109
                                    <br>: layanan.peserta@alamin.co.id
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-5 col-4" style="padding-top: 15px">
            <div class="row">
                <!-- <h6>Tentang Kami</h6> -->
                <div class="col-lg-4">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link link-dark" href="#">Profile</a>
                            <a class="nav-link link-dark" href="#">Tenaga Ahli</a>
                            <a class="nav-link link-dark" href="#">Keanggotaan Asosiasi</a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-4">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link link-dark" href="#">Izin Usaha</a>
                            <a class="nav-link link-dark" href="#">Struktur</a>
                            <a class="nav-link link-dark" href="#">Laporan Keuangan</a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-4">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link link-dark" href="#">Manajemen</a>
                            <a class="nav-link link-dark" href="#">Penghargaan</a>
                            <a class="nav-link link-dark" href="#">FAQ</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid" style="font-size: 9px;">
    <div class="container">
        <div class="row" style="padding: 10px; background-color: #B5DEFF; box-shadow: 0px 0px 3px 0px; radius: 2px 2px 0px 0px">
            <div class="col-lg-6 col-6">
                Copyright © PT Asuransi Jiwa Syariah Al-Amin 2020
            </div>
            <div class="col-lg-6 col-6" style="text-align: right">
                <a href="https://www.facebook.com/ajsyariahalamin/"><img src="node_modules/bootstrap-icons/icons/facebook.svg" style="margin-right: 5px"></a>
                <a href="https://www.instagram.com/ajsyariahalamin/"><img src="node_modules/bootstrap-icons/icons/instagram.svg" style="margin-right: 5px"></a>
                <a href="https://www.linkedin.com/ajsyariahalamin/"><img src="node_modules/bootstrap-icons/icons/linkedin.svg" style="margin-right: 5px"></a>
                <a href="https://twitter.com/ajsyariahalamin/"><img src="node_modules/bootstrap-icons/icons/twitter.svg" style="margin-right: 5px"></a>
            </div>
        </div>
    </div>
</div>