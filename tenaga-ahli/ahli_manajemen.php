<div class="container mt-5">
    <div class="row row-cols-2 row-cols-lg-6 g-2 g-lg-3" style="font-size: 9pt">
        <div class="col">
            <div class="card border-0">
                <div class="p-1 rounded-2">
                    <img class="card-img-top" src="img/man.png" alt="Card image cap">
                    <div class="card-body">
                        <p class="card-title">Donny Meifaldi, ST,AAIJ,AIIS,QCRO</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card border-0">
                <div class="p-1">
                    <img class="card-img-top" src="img/man.png" alt="Card image cap">
                    <div class="card-body">
                        <p class="card-title">Nur Ali, A.Md.Akt, <br>SE,AAAIJ,AIIS,QCRO</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card border-0">
                <div class="p-1">
                    <img class="card-img-top" src="img/man.png" alt="Card image cap">
                    <div class="card-body">
                        <p class="card-title">Suwahyono, SE,AAAIJ,AIIS,QCRO</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card border-0">
                <div class="p-1">
                    <img class="card-img-top" src="img/man.png" alt="Card image cap">
                    <div class="card-body">
                        <p class="card-title">Imran Hakim, A.Md.Akt,SE.AS,AAAIJ,QCRO</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card border-0">
                <div class="p-1">
                    <img class="card-img-top" src="img/man.png" alt="Card image cap">
                    <div class="card-body">
                        <p class="card-title">Dadang Priyandanu, A.md.Akt,AAAIJ,<br>QCRO</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card border-0">
                <div class="p-1">
                    <img class="card-img-top" src="img/man.png" alt="Card image cap">
                    <div class="card-body">
                        <p class="card-title">Herdian, A.Md.Akt,SE,AAAIJ,<br>AIIS,QCRO</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card border-0">
                <div class="p-1">
                    <img class="card-img-top" src="img/man.png" alt="Card image cap">
                    <div class="card-body">
                        <p class="card-title">Arumi Dewi, A.Md.Akt,AAAIJ</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card border-0">
                <div class="p-1">
                    <img class="card-img-top" src="img/man.png" alt="Card image cap">
                    <div class="card-body">
                        <p class="card-title">Taufik Ramdan, AAAIJ</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>