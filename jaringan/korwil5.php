<div class="container mt-3">
    <div class="row row-cols-2 row-cols-lg-4 g-2 g-lg-3" style="font-size: 9pt">
        <div class="col">
            <div class="p-3">
                <h6>Kantor Cabang Jakarta</h6>
                <p>Kepala Kantor: Ely Listyarini
                    <br>Gedung Al Amin. Jl. Sultan Agung No.12 Lt.2 Setia Budi - Jakarta Selatan 12980
                    <br>Telp. (021) 83793928 Fax.(021) 83705417
                </p>
            </div>

        </div>
        <div class="col">
            <div class="p-3">
                <h6>Kantor Cabang Bandung</h6>
                <p>
                    Kepala kantor: M Iqbal Hanafi
                    <br>Jl. Soekarna - Hatta No.590 Bandung 40286
                    <br>Telp. (022) 7536263 Fax. (022) 7537568
                </p>
            </div>
        </div>
        <div class="col">
            <div class="p-3">
                <h6>Kantor Perwakilan Bogor</h6>
                <p>
                    Kepala Kantor: Uki
                    <br>Jl. Raya Mayor Oking Komplek Centra Ruko Cibinong Blok A No.12B
                    Kel. Ciriung, Kec. Cibinong, Kab.Bogor 16918
                    <br>Telp. (021) 87910624
                </p>
            </div>
        </div>
        <div class="col">
            <div class="p-3">
                <h6>Kantor Pemasaran Cirebon</h6>
                <p>
                    Kepala Kantor: Bayu Indra, SE
                    <br>Rko Pemuda Estate Blok A No.2 Jl. Pemuda Sunyaragi - Cirebon 45113
                    <br>Telp. (0231) 8800637
                </p>
            </div>
        </div>
        <div class="col">
            <div class="p-3">
                <h6>Kantor Pemasaran Tangerang</h6>
                <p>
                    Kepala Kantor: Kamal Bahano
                    <br>Jl. Boolevard Graha Raya Ruko Orline 1 Blok ja_12
                    Kel.Paku jaya, Kec. Serpong Utara, Tangerang Selatan 15324
                    <br>Telp. (021) 53139823
                </p>
            </div>
        </div>
    </div>
</div>