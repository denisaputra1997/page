<div class="container mt-3">
    <div class="row row-cols-2 row-cols-lg-4 g-2 g-lg-3" style="font-size: 9pt">
        <div class="col">
            <div class="p-3">
                <h6>Kantor Pemasaran Aceh </h6>
                <p>Kepala Kantor: Ade Herdiyat
                    <br>Jl. Prof. Dr. Tgk Muhammad Hasan No.6B Banda Aceh
                    <br>Telp/fax. (0651)7315818
                </p>
            </div>

        </div>
        <div class="col">
            <div class="p-3">
                <h6>Kantor Pemasaran Batam</h6>
                <p>
                    Kepala kantor: Dani Kurniawan
                    <br>Graha Nagoya Mas Lt.1 R.105 Jl. Imam Bonjol nagoya Batam 29432
                    <br>Telp. 0778-421720
                </p>
            </div>
        </div>
        <div class="col">
            <div class="p-3">
                <h6>Kantor Pemasaran Bengkulu</h6>
                <p>
                    Kepala Kantor: Chairunnisa Dermawanty
                    <br>Jl. Flamboyan Raya No.82 Rt.13 Rw.04 Kel.kebun Kenangan Kec.Ratu Agung, Kota
                    Bengkulu
                    <br>Telp/Fax. (0736)732497
                </p>
            </div>
        </div>
        <div class="col">
            <div class="p-3">
                <h6>Kantor Pemasaran Pekanbaru</h6>
                <p>
                    Kepala Kantor: Nazarudin
                    <br>Jl. Datuk Maharja, Komplek Grand Sudirman Blok C No.7, Pekanbaru riau
                    <br>Telp/Fax. (0761)47612
                </p>
            </div>
        </div>
        <div class="col">
            <div class="p-3">
                <h6>Kantor Pemasaran Palembang</h6>
                <p>
                    Kepala Kantor: M. Farhan Fauzi
                    <br>Jl. Letjen Harun Sohar (Tanjung Api-api) No.159 Rt.45 Rw.10 Kel. Kebun Bunga, Kec.Sukaramai,
                    Kota Palembang 30152
                    <br>Telp. 0711-5610896 Fax. 0711-5610062
                </p>
            </div>
        </div>
        <div class="col">
            <div class="p-3">
                <h6>Kantor Pemasaran Padang</h6>
                <p>
                    Kepala Kantor: M. Andhrov
                    <br>Jl. Raya Pondok Kopi Siteba No.14 Rt.003 Rw.002 Surau Gadang-Nanggalo Padang Sumatera Barat
                    <br>Telp. (0751) 444355 Fax.(0751) 445071
                </p>
            </div>
        </div>
        <div class="col">
            <div class="p-3">
                <h6>Kantor Cabang Medan</h6>
                <p>
                    Kepala Kantor: Azvin Nur
                    <br>Jl. Sisingamangaraja No.66 kel.Masjid, Kec. Medan Kota, Sumatera Utara 20213
                    <br>Telp. (061) 42904161 Fax. (061) 42903660
                </p>
            </div>
        </div>
        <div class="col">
            <div class="p-3">
                <h6>Kantor Pemasaran Jambi</h6>
                <p>
                    Kepala Kantor: Kanti Widyastuti
                    <br>Jl. RE. Maretadinata No.51 Kel.Telanaipura Kec.Telanaipura Kota Jambi 36122
                    <br>Telp. (0741) 3073970
                </p>
            </div>
        </div>
        <div class="col">
            <div class="p-3">
                <h6>Kantor Pemasaran Lampung</h6>
                <p>
                    Kepala Kantor: Zakwan Efendi
                    <br>Office Park Way Halim, Jl. Sultan Agung No.20 Way Halim Bandar Lampung
                    <br>Telp/fax. (0721) 785626
                </p>
            </div>
        </div>
    </div>
</div>