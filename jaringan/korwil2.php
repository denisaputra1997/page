<div class="container mt-3">
    <div class="row row-cols-2 row-cols-lg-4 g-2 g-lg-3" style="font-size: 9pt">
        <div class="col">
            <div class="p-3">
                <h6>Kantor Pemasaran Banjarmasin </h6>
                <br>Komp. Ruko Jalan Lingkar Dalam Utara Sultan Adam-Benua Anyar
                No.9 Rt.23 Rw.02 Banjarmasin, Kalimantan Selatan
                <br>Telp. 0511-4315664 Fax.0511-4315713
                </p>
            </div>

        </div>
        <div class="col">
            <div class="p-3">
                <h6>Kantor Cabang Pontianak</h6>
                <p>
                    Kepala kantor: Yenny, SE
                    <br>Jl. Putri Candramidi No.31 Kel. Sungai Bangkong Kec.Pontianak, Kota Pontianak
                    <br>Telp/fax. (0561) 576385
                </p>
            </div>
        </div>
        <div class="col">
            <div class="p-3">
                <h6>Kantor Pemasaran Samarinda</h6>
                <p>
                    Kepala Kantor: Muhammad Khabirullah
                    <br>Jl. Sirad Salman No.88 (Samping Percetakan Foto Copy Konica) Samarinda-Kalimantan Timur
                    <br>Telp. 0541 (743727)
                </p>
            </div>
        </div>
    </div>
</div>