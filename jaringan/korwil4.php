<div class="container mt-3">
    <div class="row row-cols-2 row-cols-lg-4 g-2 g-lg-3" style="font-size: 9pt">
        <div class="col">
            <div class="p-3">
                <h6>Kantor Pemasaran Mataram </h6>
                <p>Kepala Kantor: Ruplan Hadi
                    <br>Jl. Catur Warga No.8 B Mataram Timur, Kota Mataram 83121
                    <br>Telp. (0370) 7845909, (0370) 7845989
                </p>
            </div>

        </div>
        <div class="col">
            <div class="p-3">
                <h6>Kantor Pemasaran Jember</h6>
                <p>
                    Kepala kantor: Ajid Try Wahono
                    <br>Komp. Ruko Elpasindo Blok N-59. Jl.Mastrip No.59 Jember - Jawa Timur
                    <br>Telp/fax. (0331) 5101260
                </p>
            </div>
        </div>
        <div class="col">
            <div class="p-3">
                <h6>Kantor Pemasaran Kediri</h6>
                <p>
                    Kepala Kantor: Budi Setiawan
                    <br>Jl. Ahmad Yani No.153 Kel.Ngadirejo Kec. Kota Rt.01 Rw.01 Kota Kediri
                </p>
            </div>
        </div>
        <div class="col">
            <div class="p-3">
                <h6>Kantor Perwakilan Yogyakarta</h6>
                <p>
                    Kepala Kantor: Tri Wahyu Utomo
                    <br>Jl. Ringroad Timur Ruko Merah No.06 Rt.08 Rw.32 Ketandan-Banguntandan Yogyakarta - 55198
                    <br>Telp.(0274) 2840530 Fax.(0274) 2840692
                </p>
            </div>
        </div>
        <div class="col">
            <div class="p-3">
                <h6>Kantor Cabang Semarang</h6>
                <p>
                    Kepala Kantor: Jemmy Agustina
                    <br>Komp. Ruko Grand Ngaliyan Square C-43 Jl.Raya Prof. DR. Hamka Kel. Purwayoso Kec.Ngaliyan
                    <br>Telp. (024) 76638046 Fax.(024) 76638208
                </p>
            </div>
        </div>
        <div class="col">
            <div class="p-3">
                <h6>Kantor Cabang Surabaya</h6>
                <p>
                    Kepala Kantor: Suwandodo
                    <br>Ruko Surya Inti Permata I Blok B-30 Jl. HR. Muhammad No.177 Surabaya
                    <br>Telp. (031)7343657,7347993,7345689 Fax.(031)7345690
                </p>
            </div>
        </div>
    </div>
</div>