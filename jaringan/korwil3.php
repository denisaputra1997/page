<div class="container mt-3">
    <div class="row row-cols-2 row-cols-lg-4 g-2 g-lg-3" style="font-size: 9pt">
        <div class="col">
            <div class="p-3">
                <h6>Kantor Pemasaran Ternate </h6>
                <p>Kepala Kantor: Supriadi Sjamsuddin
                    <br>Jl. Raya Jati (Samping Hotel Grand Dafam Bela Ternate) Kel. Jati, Kec. ternate Selatan, Kota
                    Ternate- Maluku Utara 97716
                    <br>Telp. 0852-4242-7124
                </p>
            </div>

        </div>
        <div class="col">
            <div class="p-3">
                <h6>Kantor Cabang Makasar</h6>
                <p>
                    Kepala kantor: Herpiana
                    <br>Jl. Andi Raya Mappoddang No.43B (Depan SMA 11 Makasar) Jongaya, Tamalate, Sulawesi Selatan 90223
                    <br>Telp. 0411 (8911082)
                </p>
            </div>
        </div>
        <div class="col">
            <div class="p-3">
                <h6>Kantor Pemasaran Palu</h6>
                <p>
                    Kepala Kantor: Jayadi Jamaludin
                    <br>Jl. Pue Bongo No.97 A Kel.Pengawu, Kec.Tatanga Kota Palu-Sulawesi Tengah 94129
                    <br>Telp/fax. (0451) 4014778
                </p>
            </div>
        </div>
    </div>
</div>