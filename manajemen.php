<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="shortcut icon" type="image/x-icon" href="img/logo.png" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Mochiy+Pop+P+One&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Fredoka+One&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Kurale&family=Quicksand:wght@700&display=swap"
        rel="stylesheet">
    <title>Asuransi Jiwa Al-Amin Syariah</title>
</head>

<body>
    <div class="container">
        <?php include 'nav.php' ?>
    </div>
    <header class="container-fluid"
        style="background-image: url('img/background.jpg'); background-position: cover; background-size: 100%; background-repeat: no-repeat; height: 30%; ">
        <div class="container">
            <div class="row  align-items-center">
                <div class="col-lg-4 col-4 col text-center ">
                    <img src="img/office-building.png" width="60%">
                </div>
                <div class="col-lg-6 col-6 col">

                    <p>
                    <h6>KANTOR PUSAT</h6>
                    Jl. Sultan Agung No. 12A Kel. Guntur, Kec. Setiabudi,
                    <br>Jakarta Selatan 12980
                    </p>
                </div>
            </div>
        </div>
    </header>

    <main class="container my-4">
        <h5 class="text-Left judul-layanan">Jaringan Layanan</h5>
        <hr>
        <ul class="nav nav-tabs item-layanan">
            <li class="nav-item">
                <a class="nav-link active " data-bs-toggle="tab" href="#korwil1">Dewan Komisaris</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-bs-toggle="tab" href="#korwil2">Dewan Pengawas Syariah</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-bs-toggle="tab" href="#korwil3">Dewan Direksi</a>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content text-dark">
            <div class="tab-pane container active" id="korwil1">
                <div class="container mt-5">
                    <div class="row row-cols-2 row-cols-lg-2 g-2 g-lg-2" style="font-size: 9pt">
                        <div class="col">
                            <div class="card border-0">
                                <div class="p-1">
                                    <img class="card-img-top" src="img/komisaris_amin.jpg" alt="Card image cap">
                                    <div class="card-body">
                                        <p class="card-title">
                                            <br>H.M. Amin Anggianto, AMRP
                                            <br>Komisaris Utama
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="card border-0">
                                <div class="p-1">
                                    <img class="card-img-top" src="img/komisaris.jpg" alt="Card image cap">
                                    <div class="card-body">
                                        <p class="card-title">
                                            <br>Farah Octavia, MPA, ANZIIF (Fellow), AMRP
                                            <br>Komisaris
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row row-cols-2 row-cols-lg-3 g-2 g-lg-3" style="font-size: 9pt">
                        <div class="col">
                            <div class="card border-0">
                                <div class="p-1">
                                    <img class="card-img-top" src="img/soekotjo.jpg" alt="Card image cap">
                                    <div class="card-body">
                                        <p class="card-title">
                                            <br>Soekotjo Soeparto, S.H., LL.M.
                                            <br>Komisaris Independen
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="card border-0">
                                <div class="p-1">
                                    <img class="card-img-top" src="img/bar-i.jpg" alt="Card image cap">
                                    <div class="card-body">
                                        <p class="card-title">
                                            <br>Drs. Mohammad Bar'i, MA
                                            <br>Komisaris Independen
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="card border-0">
                                <div class="p-1">
                                    <img class="card-img-top" src="img/khairi.jpg" alt="Card image cap">
                                    <div class="card-body">
                                        <p class="card-title">
                                            <br>Drs. Syafwanul Khairi, AAAIK
                                            <br>Komisaris Independen
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane container fade" id="korwil2">
                <div class="container mt-5">
                    <div class="row row-cols-2 row-cols-lg-3 g-2 g-lg-3" style="font-size: 9pt">
                        <div class="col">
                            <div class="card border-0">
                                <div class="p-1">
                                    <img class="card-img-top" src="img/man.png" alt="Card image cap">
                                    <div class="card-body">
                                        <p class="card-title">
                                            <br>Drs. H.M. Ichwan Sam
                                            <br>-
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="card border-0">
                                <div class="p-1">
                                    <img class="card-img-top" src="img/man.png" alt="Card image cap">
                                    <div class="card-body">
                                        <p class="card-title">
                                            <br>Prof. Dr.Jaih Mubarok, SE, MH, M.Ag
                                            <br>-
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="card border-0">
                                <div class="p-1">
                                    <img class="card-img-top" src="img/man.png" alt="Card image cap">
                                    <div class="card-body">
                                        <p class="card-title">
                                            <br>Abdul Wasik, M.Si
                                            <br>-
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane container fade" id="korwil3">
                <!-- <?php include 'jaringan/korwil3.php'; ?> -->
            </div>
            <div class="tab-pane container fade" id="korwil4">
                <!-- <?php include 'jaringan/korwil4.php'; ?> -->
            </div>
            <div class="tab-pane container fade" id="korwil5">
                <!-- <?php include 'jaringan/korwil5.php'; ?> -->
            </div>
        </div>
    </main>
    <?php include 'toTop.php' ?>
    <footer class="container-fluid" id="footer" style="margin-top: 30px; background-color: #D7E9F7">
        <?php include 'footer.php' ?>
    </footer>

</body>

<!-- Optional JavaScript; choose one of the two! -->

<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
</script>
</body>

</html>
<style>
.item-layanan a {
    color: black;
}

.navbar-nav .nav-item a {
    color: #11468F;
}

.navbar-nav .nav-item a:hover {
    color: #9AD0EC;
}

.navbar-nav .nav-item a:active {
    color: #9AD0EC;
}

body {
    font-family: 'Quicksand', sans-serif;
}

header .container .row .col {
    margin-top: 5%;
    margin-bottom: 5%;
}

header .container .row .col-6 {
    font-size: 10pt;
}
</style>