<div class="row mt-5" id="produk">
    <div class="col-lg-3 col-6 justify-content-center">
        <div class="container my-3" id="product">
            <a href="#" data-bs-toggle="modal" data-bs-target="#myModal1">
                <img class="image" src="img/makro1.jpg" alt="Avatar">
                <!-- <div class="overlay">At-ta'min Pembiayaan <br>Mikro</div> -->
            </a>
        </div>
    </div>

    <div class="col-lg-3 col-6">
        <div class="container my-3" id="product">
            <a href="#" data-bs-toggle="modal" data-bs-target="#myModal2">
                <img class="image" src="img/siswa_dinar1.jpg" alt="Avatar">
                <!-- <div class="overlay">At-ta'min Siswa Dinar</div> -->
            </a>
        </div>

    </div>
    <div class="col-lg-3 col-6">
        <div class="container my-3" id="product">
            <a href="#" data-bs-toggle="modal" data-bs-target="#myModal3">
                <img class="image" src="img/siswa_dirham1.jpg" alt="Avatar">
                <!-- <div class="overlay">At-ta'min Siswa Dirham</div> -->
            </a>
        </div>

    </div>
    <div class="col-lg-3 col-6">
        <div class="container my-3" id="product">
            <a href="#" data-bs-toggle="modal" data-bs-target="#myModal4">
                <img class="image" src="img/arafah1.jpg" alt="Avatar">
                <!-- <div class="overlay">Al Amin Badal Arafah</div> -->
            </a>
        </div>
    </div>

    <!-- The Modal -->
    <div class="modal fade" role="dialog" tabindex="-1" id="myModal1">
        <div class="modal-dialog"> 
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h6 class="modal-title">AT TA'MIN PEMBIAYAAN MIKRO</h6>
                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                </div>

                <!-- Modal body -->
                <div class="modal-body" style="text-align: left; font-size: 8pt;">
                    <p> Program asuransi syariah yang memberikan perlindungan
                        atau jaminan penggantian kerugian finansial kepada
                        Penerima Manfaat apabila Peserta Yang Diasuransikan
                        dalam masa perlindungan asuransi syariah tidak dapat
                        memenuhi kewajiban untuk melunasi pembiayaannya
                        akibat mengalami risiko yang dijamin pada Polis.
                        <br>&nbsp;
                        <br>Resiko Yang Dijamin : Meninggal Dunia
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
    <!-- Modal end -->

    <!-- The Modal -->
    <div class="modal fade" id="myModal2">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">AT-TA'MIN SISWA DINAR</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                </div>

                <!-- Modal body -->
                <div class="modal-body" style="text-align: left; font-size: 8pt;">
                    <p> Program asuransi syariah yang ditujukan untuk Siswa atau Mahasiswa dengan memberikan manfaat
                        kepada Penerima Manfaat apabila Peserta Yang Diasuransikan dalam jangka waktu perlindungan
                        Asuransi Syariah mengalami risiko meninggal dunia akibat sakit/kecelakaan, atau mengalami Cacat
                        Tetap akibat Kecelakaan, atau Perawatan di rumah sakit akibat Kecelakaan dengan penggantian
                        sebesar yang tercantum pada Daftar Kepesertaan Asuransi Syariah dan/atau Polis.


                        <br>&nbsp;
                        <br>Resio Yang Dijamin : Meninggal Dunia, PA (ABD), Rawat Inap
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
    <!-- Modal end -->

    <!-- The Modal -->
    <div class="modal fade" id="myModal3">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">AT-TAMIN SISWA DIRHAM</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                </div>

                <!-- Modal body -->
                <div class="modal-body" style="text-align: left; font-size: 8pt;">
                    <p> Program asuransi syariah yang ditujukan untuk Siswa atau Mahasiswa dengan memberikan Manfaat
                        Asuransi Syariah kepada Penerima Manfaat apabila Peserta Yang Diasuransikan dalam jangka waktu
                        perlindungan Asuransi Syariah mengalami risiko Meninggal Dunia akibat Kecelakaan, atau mengalami
                        Cacat Tetap akibat Kecelakaan, atau Perawatan di rumah sakit akibat kecelakaan dengan
                        penggantian sebesar yang tercantum pada Daftar Kepesertaan Asuransi Syariah dan/atau Polis.
                        <br>&nbsp;
                        <br>Resio Yang Dijamin : PA (ABD), Rawat Inap
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
    <!-- Modal end -->

    <!-- The Modal -->
    <div class="modal fade" id="myModal4">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">AL-AMIN BADAL ARAFAH</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                </div>

                <!-- Modal body -->
                <div class="modal-body" style="text-align: left; font-size: 8pt;">
                    <p> Program Asuransi Jiwa Syariah yang memberikan Manfaat Asuransi Syariah kepada Penerima Manfaat
                        dan pembiayaan ibadah Haji (Badal Haji) bagi Peserta Yang Diasuransikan sebesar yang tercantum
                        pada Polis jika Peserta Yang Diasuransikan mengalami risiko yang dijamin dalam masa asuransi
                        syariah berupa meninggal dunia karena sakit maupun kecelakaan.


                        <br>&nbsp;
                        <br>Resiko Yang Dijamin : Meninggal Dunia Biasa, PA (ABD), Rawat Inap
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
    <!-- Modal end -->

</div>

<style>
* {
    box-sizing: border-box;
}

/* Container needed to position the overlay. Adjust the width as needed */
#product {
    position: relative;
    width: 100%;
}

/* Make the image to responsive */
#product .image {
    display: block;
    width: 100%;
    height: auto;
    border-radius: 50px;
    transition: 0.3s;
}

/* Make the hover at box image */
#product:hover .image {
    width: 110%;
    box-shadow: 3px 3px 3px 1px grey;
}

/* When you mouse over the container, fade in the overlay title */
#product:hover .overlay {
    opacity: 1;
}
</style>