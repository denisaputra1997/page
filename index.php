<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="shortcut icon" type="image/x-icon" href="img/logo.png" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Mochiy+Pop+P+One&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Fredoka+One&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Kurale&family=Quicksand:wght@700&display=swap"
        rel="stylesheet">
    <title>Asuransi Jiwa Al-Amin Syariah</title>


</head>

<body>
    <div class="container">
        <?php include 'nav.php' ?>
    </div>
    <header class="container-fluid text-center" id="header-laporan"
        style="background-image: url('img/background.jpg'); background-position: cover; background-size: 100%; background-repeat: no-repeat;">
        <div class="container">
            <div class="row pb-4  align-items-center">
                <div class="col col-lg-6 col-6" style="font-family: 'Mochiy Pop P One', sans-serif; ">
                    Perlindungan yang amanah
                    <br>dan terpercaya
                </div>
                <div class="col col-lg-6 col-6 text-center">
                    <img src="img/insurance.png" width="35%">
                </div>
            </div>
        </div>
    </header>


    <main class="container">
        <!-- produk perusahaan -->
        <div class="container py-5 my-5 profile" id="produk">
            <h4 style="text-align: center;">Produk Perusahaan</h4>
            <hr class="bg-dark border-2 border-top border-dark me-auto ms-auto" width="70%">
            <div class="container text-center">
                <?php include 'product.php'; ?>
            </div>
        </div>

        <!-- profile perusahaan -->
        <!-- <div class="container py-3 my-3" id="profil">
            <h3 style="text-align: center;">Profil</h3>
            <hr class="bg-dark border-2 border-top border-dark me-auto ms-auto" width="70%">
            <div class="container text-center">
                <?php include 'tentang_kami/profil.php'; ?>
            </div>
        </div> -->
    </main>

    <?php include 'toTop.php' ?>


    </div>
    <footer class="container-fluid" id="footer" style="margin-top: 30px; background-color: #D7E9F7">

        <?php include 'footer.php' ?>
    </footer>

</body>

<!-- Optional JavaScript; choose one of the two! -->

<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
</script>

<!-- Option 2: Separate Popper and Bootstrap JS -->
<!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->
</body>

</html>

<style>
body {
    font-family: 'Quicksand', sans-serif;
}

#header-laporan .row .col {
    margin-top: 8%;
    margin-bottom: 8%;
    
}
</style>