<div class="row">
    <div class="col-6 col-lg-6 mt-5">
        <h5>VISI</h5>
        <hr class="bg-dark border-2 border-top border-dark me-auto ms-auto" width="30%">
        <p>"Menjadi Perusahaan Asuransi Jiwa Syarian yang Handal dan Terpercaya"</p>
    </div>
    <div class="col-6 col-lg-6 mt-5">
        <h5>MISI</h5>
        <hr class="bg-dark border-2 border-top border-dark me-auto ms-auto" width="30%">
        <p>"Memberikan Pelayanan yang terbaik kepada nasabah dengan melaksanakan pengelolaan manajemen risiko yang
            sehat"</p>
    </div>

    <div class="col-12 col-lg-12 mt-5">
        <h5>MOTO</h5>
        <hr class="bg-dark border-2 border-top border-dark me-auto ms-auto" width="30%">
        <p>"Perlindungan yang Amanah dan Terpercaya"</p>
    </div>
</div>