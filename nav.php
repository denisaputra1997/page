<nav class="navbar navbar-expand-lg" style="font-size: 13px" id="navbar">
    <div class="container-fluid ">
        <a class="navbar-brand" style="font-size: 12pt; color: #11468F" href="/alamin/page"><img src="img/logo.png" width="50px">
        PT. Asuransi Jiwa Syariah Al-Amin</a>
        <button class="navbar-toggler navbar-light" type="button" data-bs-toggle="collapse"
            data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarScroll">
            <ul class="navbar-nav ms-auto my-2 my-lg-0 navbar-nav-scroll"
                style="--bs-scroll-height: 100px; font-family: 'Quicksand', sans-serif;">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="../page/">Beranda</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" role="button" aria-hashpopup="true" id="dropdownMenuLink"
                        data-bs-toggle="dropdown" aria-expanded="false">Tentang Kami</a>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">

                        <li> <a class="dropdown-item" href="#profil"> Profile</a></li>

                        <li> <a class="dropdown-item" href="tenaga_ahli.php"> Tenaga Ahli</a></li>

                        <li> <a class="dropdown-item" href="#"> Keanggotaan Asosiasi (x)</a></li>

                        <li> <a class="dropdown-item" href="#"> Izin Usaha (x)</a></li>

                        <li> <a class="dropdown-item" href="struktur.php"> Struktur</a></li>

                        <li> <a class="dropdown-item" href="laporan.php"> Laporan Keuangan</a></li>

                        <li> <a class="dropdown-item" href="manajemen.php"> Manajemen</a></li>

                        <li> <a class="dropdown-item" href="#"> Penghargaan (x)</a></li>

                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/alamin/page/#produk">Produk</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="jaringan.php">Jaringan</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Media (x)</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="hubungi_kami.php">Hubungan Kami</a>
                </li>
            </ul>
        </div>
    </div>
</nav>


<style>
.dropdown:hover>.dropdown-menu {
    display: block;
    font-size: 12px;
    transition: 0.3s;
}

.dropdown>.nav-link:active {
    pointer-events: none;

}

.dropdown>.dropdown-menu {
    transition: 0.3s;
}

.container>#profil p {
    font-family: 'Mochiy Pop P One', sans-serif;
    font-size: 10pt;
}

.navbar-nav .nav-item a {
    color: #11468F;
    transition: 0.3s;
}

.navbar-nav .nav-item a:hover {
    color: #9AD0EC;
}

.navbar-nav .nav-item a:active {
    color: #9AD0EC;
}

</style>
<!-- 
<script>
window.onscroll = function() {
    scrollFunction()
};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("navbar").style.top = "0";
    } else {
        document.getElementById("navbar").style.top = "-50px";
    }
}
</script> -->